# README

This file provides documentation on what the script does, how to install the required packages, and how to run the script.

## Image to ASCII Art Converter (image_to_ascii.py)

This Python script converts an image into ASCII art and saves it as a text file.

### Requirements

- Python 3.x
- Pillow

### Installation

First, you need to install the required packages:

```bash
pip install -r requirements.txt
```

### Usage

Run the script from the command line by passing the image path as an argument:

```bash
python image_to_ascii.py frajder.jpg
```

This will create a text file named [frajder.txt](frajder.txt)  in the same directory as the input image [frajder.jpg](frajder.jpg), containing the ASCII art representation of the image.

### Viewing the ASCII Art

You can view the ASCII art using `cat` or any text editor:

```bash
cat input_image.txt
```
Enjoy your ASCII art!
